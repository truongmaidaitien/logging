[TOC]

***********************************************************

# How to use

Copy env-example to .env

Edit setting in env, replace if need:
```
WORKSPACE_FLUENTD_CONFIG=./fluentd/conf/
WORKSPACE_LOG_PATH=/var/log/my-project/logs
WORKSPACE_ELASTICSEARCH_DATA=./elasticsearch/data
WORKSPACE_GRAFANA_DATA=./grafana
CONTAINER_LOG_PATH=/var/log/my-project/logs
```

***********************************************************

# Manual setup Fluentd

[Full document here](https://docs.fluentd.org/v1.0/articles/quickstart)

### [Before Installation](https://docs.fluentd.org/v1.0/articles/before-install)
* Set Up NTP
* Increase Max # of File Descriptors
* Optimize Network Kernel Parameters

### Install from deb package
For Ubuntu Trusty
```
curl -L https://toolbelt.treasuredata.com/sh/install-ubuntu-trusty-td-agent3.sh | sh
```

### Config file
```
/etc/td-agent/td-agent.conf
```

### Run
```
sudo /etc/init.d/td-agent start
```

### Stop
```
sudo /etc/init.d/td-agent stop
```

### Autostart
```
sudo update-rc.d td-agent defaults
```
if you get
```
System start/stop links for /etc/init.d/td-agent already exist.
```
Do the command
```
sudo update-rc.d td-agent enable
```

### Install plugins
[read this doc](https://docs.fluentd.org/v1.0/articles/plugin-management#if-using-td-agent,-use-/usr/sbin/td-agent-gem)

Need 2 following plugins to send log to Elasticsearch
```
sudo /usr/sbin/td-agent-gem install fluent-plugin-elasticsearch
sudo /usr/sbin/td-agent-gem install fluent-plugin-record-reformer
```

### Example

```xml
<source>
    @type tail
    @id myproject_input_file
    tag myproject.input.file
 
    multiline_flush_interval 15s
 
    path /var/www/my-project/logs/laravel.log # change path to log file
    pos_file /var/log/td-agent/my-project/laravel.log.pos # can change dir
 
    <parse>
        @type multiline
        format_firstline /^\[\d{4}-\d{1,2}-\d{1,2}\s\d{1,2}:\d{1,2}:\d{1,2}\].*/
        format1 /.+(?<level>FATAL|ERROR|WARN|INFO|DEBUG|TRACE):\s(?<message>.*).+\{.+\[object\].+\((?<class>.*)\(code:\s(?<status>\d{1,3})\):\s(?<location>.*)\)[\r\n]+\[stacktrace\]/
    </parse>
</source>
 
<match myproject.input.file>
    @type record_reformer
    @id output_reformer
    tag output.reformer
</match>
 
<match output.reformer>
    @type elasticsearch
    @id output_elasticsearch
     
    host elasticsearch
    port 9200
    logstash_format true
    logstash_prefix fluentd-log
    type_name fluentd
 
    <buffer>
        flush_interval 15s
        flush_mode interval
    </buffer>
</match>
```

### Description

```<source>``` là dữ liệu đầu vào, gọi là input plugin.  
```@type```: Loại input, bắt buộc phải định nghĩa. Ở đây dùng tail để tracking và nhận dữ liệu từ file log https://docs.fluentd.org/v1.0/articles/in_tail   
```@id```: ID của plugin, không bắt buộc   
```tag```: Dùng để điều hướng, các input và output plugin sẽ dùng tag để nhận biết dữ liệu được nhận  
```multiline_flush_interval```: Thời gian flush buffer (dùng cho multiline của ```<parse>```, Phải định nghĩa để phần ```<buffer>``` ở dưới chạy chính xác).    
```path```: Đường dẫn đến file cần đọc    
```pos_file```: Lưu vị trí đọc cuối cùng từ path     
```<parse>``` định nghĩa regex để đọc dữ liệu https://docs.fluentd.org/v1.0/articles/parser-plugin-overview, Chú ý named capture ```(?<NAME>PATTERN)```  
   
```<match>``` là dữ liệu đầu ra, gọi là output plugin  
```@type```: Loại output, bắt buộc phải định nghĩa. Có thể cài thêm plugin ví dụ như fluent-plugin-record-reformer, fluent-plugin-elasticsearch  
```@id```: ID của plugin, không bắt buộc  
Các trường host, port, logstash_format, logstash_prefix, type_name là config của fluent-plugin-elasticsearch, tham khảo https://docs.fluentd.org/v1.0/articles/out_elasticsearch   
```<buffer>``` bộ đệm, tham khảo https://docs.fluentd.org/v1.0/articles/buffer-section, ở đây dùng flush_mode interval để gởi dữ liệu sau 1 khoảng thời gian.